package com.epam.training;

import java.util.Arrays;
import java.util.Comparator;

public class MyPriorityQueue<E> {
    private static final int INITIAL_CAPACITY = 11;
    private E[] queue;
    private int size = 0;
    private final Comparator<? super E> comparator;


    public MyPriorityQueue() {
        comparator = null;
        queue = (E[]) new Object[INITIAL_CAPACITY];
    }

    public MyPriorityQueue(Comparator<? super E> comparator) {
        this.comparator = comparator;
    }

    private void grow() {
        int oldCapacity = queue.length;
        int newCapacity = Integer.parseInt(Double.toString(oldCapacity * 1.5));
        queue = Arrays.copyOf(queue, newCapacity);
    }

    public boolean add(E e) {
        return offer(e);
    }

    public boolean offer(E e) {
        if (e == null)
            throw new NullPointerException();
        int i = size;
        if (i >= queue.length)
            grow();
        size++;
        if (i == 0)
            queue[0] = e;
        else
            siftUp(i, e);
        return true;
    }

    private void siftUp(int k, E x) {
        if (comparator != null)
            siftUpUsingComparator(k, x);
        else
            siftUpComparable(k, x);
    }

    private void siftUpComparable(int k, E x) {
        Comparable<? super E> key = (Comparable<? super E>) x;
        while (k > 0) {
            int previous = k - 1;
            E e = queue[previous];
            if (key.compareTo(e) >= 0)
                break;
            queue[k] = (E) e;
            k = previous;
        }
        queue[k] = (E) key;
    }

    private void siftUpUsingComparator(int k, E x) {
        while (k > 0) {
            int previous = k - 1;
            E e = queue[previous];
            if (comparator.compare(x, e) >= 0)
                break;
            queue[k] = (E) e;
            k = previous;
        }
        queue[k] = x;
    }

    private int indexOf(E o) {
        if (o != null) {
            for (int i = 0; i < size; i++)
                if (o.equals(queue[i]))
                    return i;
        }
        return -1;
    }

    public int size() {
        return size;
    }

    public void clear() {
        for (int i = 0; i < size; i++)
            queue[i] = null;
        size = 0;
    }

    public E poll() {
        if (size == 0)
            return null;
        int s = --size;
        E result = queue[0];
        E x = queue[s];
        queue[s] = null;
        if (s != 0)
            siftDown(0, x);
        return result;
    }

    public boolean remove(){
        E deleted = poll();
        if (deleted != null){
            return true;
        }else{
            return false;
        }
    }

    private void siftDown(int k, E x) {
        if (comparator != null)
            siftDownUsingComparator(k, x);
        else
            siftDownComparable(k, x);
    }

    private void siftDownComparable(int k, E x) {
        Comparable<? super E> key = (Comparable<? super E>) x;
        queue[k] = queue[k+1];
        while (k < (size - 1)) {
            int next = k + 1;
            E c = queue[next];
            int right = next + 1;
            if (right < size &&
                    ((Comparable<? super E>) c).compareTo(queue[right]) > 0)
                c = queue[next = right];
            if (key.compareTo(c) <= 0)
                break;
            queue[k] = c;
            k = next;
        }
        queue[k] = (E) key;
    }

    private void siftDownUsingComparator(int k, E x) {
        queue[k] = queue[k+1];
        while (k < (size - 1)) {
            int next = k + 1;
            E c = queue[next];
            int right = next + 1;
            if (right < size &&
                    comparator.compare(c, queue[right]) > 0)
                c = queue[next = right];
            if (comparator.compare(x, c) <= 0)
                break;
            queue[k] = c;
            k = next;
        }
        queue[k] = x;
    }

    public E peek() {
        return (size == 0) ? null : queue[0];
    }

    public boolean remove(E o) {
        int i = indexOf(o);
        if (i == -1)
            return false;
        else {
            removeAt(i);
            return true;
        }
    }

    private E removeAt(int i) {
        int s = --size;
        if (s == i) // removed last element
            queue[i] = null;
        else {
            E moved = queue[s];
            queue[s] = null;
            siftDown(i, moved);
            if (queue[i] == moved) {
                siftUp(i, moved);
                if (queue[i] != moved)
                    return moved;
            }
        }
        return null;
    }

    /**
     * Just for a test
     */
    public void printQueue(){
        for (int i = 0; i < size; i++) {
            Main.logger.info(queue[i]);
        }
    }
}
