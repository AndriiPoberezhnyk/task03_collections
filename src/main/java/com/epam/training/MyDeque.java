package com.epam.training;

import java.util.Arrays;

public class MyDeque<E> {
    private static final int INITIAL_CAPACITY = 11;
    private E[] queue;
    private int size;
    private int head;
    private int tail;

    public MyDeque() {
        this.queue = (E[]) new Object[INITIAL_CAPACITY];
        size = 0;
        head = 0;
        tail = 0;
    }

    public MyDeque(int size) {
        this.queue = (E[]) new Object[size];
        this.size = 0;
        head = 0;
        tail = 0;
    }

    public void add(E e) {
        offer(e);
    }

    public void addFirst(E e) {
        offerFirst(e);
    }

    public void addLast(E e) {
        offerLast(e);
    }

    public boolean offerFirst(E e) {
        if (e == null)
            throw new NullPointerException();
        int i = size;
        if (i >= queue.length)
            grow();
        size++;
        if (i == 0)
            queue[0] = e;
        else {
            tail++;
            moveElementsToTheRight();
            queue[0] = e;
        }
        return true;
    }

    public boolean offerLast(E e) {
        return offer(e);
    }

    public E removeFirst() {
        return pollFirst();
    }

    public E removeLast() {
        return pollLast();
    }

    public E pollFirst() {
        E element = peekFirst();
        if (element == null)
            return null;
        moveElementsToTheLeft();
        tail--;
        size--;
        return element;
    }

    public E pollLast() {
        E removed = queue[tail];
        tail--;
        size--;
        queue = Arrays.copyOf(queue, queue.length - 1);
        return removed;
    }

    public E peekFirst() {
        return queue[head];
    }

    public E peekLast() {
        return queue[tail];
    }


    public boolean offer(E e) {
        if (e == null)
            throw new NullPointerException();
        int i = size;
        if (i >= queue.length)
            grow();
        size++;
        if (i == 0)
            queue[0] = e;
        else {
            tail++;
            queue[tail] = e;
        }
        return true;
    }

    public E poll() {
        return pollFirst();
    }

    public E peek() {
        return peekFirst();
    }

    public void push(E e) {
        addFirst(e);
    }

    public E pop() {
        E removed = queue[head];
        tail--;
        size--;
        moveElementsToTheLeft();
        return removed;
    }

    public boolean contains(E e) {
        for (int i = 0; i < size; i++) {
            if (queue[i] == e) {
                return true;
            }
        }
        return false;
    }

    public int size() {
        return size;
    }

    private void grow() {
        int oldCapacity = queue.length;
        int newCapacity = Integer.parseInt(Double.toString(oldCapacity * 1.5));
        queue = Arrays.copyOf(queue, newCapacity);
    }

    private void moveElementsToTheRight() {
        E[] elements = (E[]) new Object[queue.length];
        elements[0] = null;
        for (int i = 1; i < queue.length; i++) {
            elements[i] = queue[i - 1];
        }
        queue = Arrays.copyOf(elements, elements.length);
    }

    private void moveElementsToTheLeft() {
        E[] elements = (E[]) new Object[queue.length - 1];
        for (int i = 1; i < queue.length; i++) {
            elements[i - 1] = queue[i];
        }
        queue = Arrays.copyOf(elements, elements.length);
    }
}
