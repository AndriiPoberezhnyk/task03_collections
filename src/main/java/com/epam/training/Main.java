package com.epam.training;

import org.apache.logging.log4j.*;

import java.util.*;

public class Main {
    static final Logger logger = LogManager.getLogger(Main.class.getName());

    public static void main(String[] args) {
        testPriorityQueue();
        logger.warn("--------------------------------------------------------------");
        testDeque();
    }

    public static void testPriorityQueue(){
        MyPriorityQueue<Integer> myPriorityQueue = new MyPriorityQueue<>();
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
        logger.info(myPriorityQueue.offer(1));
        logger.info(myPriorityQueue.offer(2));
        logger.info(myPriorityQueue.offer(3));
        logger.info(myPriorityQueue.offer(1));
        logger.info(myPriorityQueue.offer(2));
        myPriorityQueue.printQueue();
        logger.info(myPriorityQueue.poll());
        myPriorityQueue.printQueue();
        logger.info(myPriorityQueue.remove(3));
        myPriorityQueue.printQueue();
        logger.info(myPriorityQueue.remove(2));
        myPriorityQueue.printQueue();
        logger.info(myPriorityQueue.offer(1));
        logger.info(myPriorityQueue.add(1));
        logger.info(myPriorityQueue.offer(3));
        logger.info(myPriorityQueue.offer(2));
        logger.info(myPriorityQueue.remove(2));
        myPriorityQueue.printQueue();
    }

    public static void testDeque(){
        MyDeque<String> myDeque = new MyDeque<>();
        logger.info(myDeque.offer("1"));
        logger.info(myDeque.offer("2"));
        logger.info(myDeque.offer("3"));
        logger.info(myDeque.offer("4"));
        logger.info(myDeque.offer("5"));
        logger.info(myDeque.contains("1"));
        logger.info(myDeque.peek());
        logger.info(myDeque.peekFirst());
        logger.info(myDeque.peekLast());
        logger.info(myDeque.poll());
        logger.info(myDeque.pollFirst());
        logger.info(myDeque.pollLast());
        logger.info(myDeque.size());
        myDeque.push("123");
        logger.info(myDeque.peekFirst());
    }

}
